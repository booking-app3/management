from base import Base
from dataclasses import dataclass
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship


@dataclass
class Facility(Base):
    __tablename__ = 'Facility'

    id: int
    facility_name: str
    comf_level_id: int

    id = Column(Integer, primary_key=True, autoincrement=True)
    facility_name = Column(String)
    comf_level_id = Column(Integer, ForeignKey('ComfortLevel.id', ondelete="CASCADE"))
    comfort_level = relationship('ComfortLevel', back_populates="facilities")
