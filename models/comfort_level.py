from base import Base
from dataclasses import dataclass
from facility import Facility
from sqlalchemy import Column, Integer
from sqlalchemy.orm import relationship



@dataclass
class ComfortLevel(Base):
    __tablename__ = 'ComfortLevel'
    __table_args__ = {'extend_existing': True}

    id: int
    comfort_level: int

    id = Column(Integer, primary_key=True, autoincrement=True)
    comfort_level = Column(Integer)
    facilities = relationship(Facility, back_populates="comfort_level", cascade="all")
