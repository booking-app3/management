from flask import Flask
from hotel_controller import hotel_controller
from city_controller import city_controller
from room_controller import room_controller
from facility_controller import facility_controller
from comfort_level_controller import comfort_level_controller
from utils import CustomJSONEncoder

app = Flask(__name__)
app.json_encoder = CustomJSONEncoder
app.register_blueprint(hotel_controller)
app.register_blueprint(city_controller)
app.register_blueprint(room_controller)
app.register_blueprint(facility_controller)
app.register_blueprint(comfort_level_controller)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug = True)