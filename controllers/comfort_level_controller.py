from flask import jsonify, Response, request, json, Blueprint
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError
from comfort_level import ComfortLevel
from utils import required_params
from session import Session

comfort_level_controller = Blueprint('comfort_level_controller', __name__, template_folder='controllers')

@comfort_level_controller.route("/api/comfort_levels", methods=['GET'])
def ger_comfort_levels():
    session = Session()
    try:
        comfort_levels = session.query(ComfortLevel).all()
    except Exception as e:
        session.rollback()
        return str(e)
    return jsonify(comfort_levels)

@comfort_level_controller.route("/api/comfort_levels", methods=['POST'])
@required_params({"comfort_level": int})
def add_comfort_level():
    data = request.get_json(silent=True)
    session = Session()
    try:
        comfort_level = ComfortLevel(comfort_level=data['comfort_level'])
        session.add(comfort_level)
        session.commit()

    except IntegrityError:
        session.rollback()
        return Response(status=409)

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        session.rollback()
        return str(e)

    return Response(json.dumps({'id': comfort_level.id}),
                    status=201,
                    mimetype='application/json')

@comfort_level_controller.route("/api/comfort_levels", methods=['PUT'])
@required_params({"id": int, "comfort_level": int})
def update_comfort_level():
    data = request.get_json(silent=True)
    session = Session()
    try:
        comfort_level = session.query(ComfortLevel).filter(ComfortLevel.id == data['id']).one()
        comfort_level.comfort_level = data['comfort_level']
        session.commit()

    except IntegrityError:
        session.rollback()
        return Response(status=409)

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        session.rollback()
        return str(e)

    return Response(json.dumps({'id': comfort_level.id}),
                    status=201,
                    mimetype='application/json')

@comfort_level_controller.route("/api/comfort_levels/<int:comfort_level_id>", methods=['DELETE'])
def delete_comfort_level(comfort_level_id):
    session = Session()
    try:
        comfort_level = session.query(ComfortLevel).filter(ComfortLevel.id == comfort_level_id).one()
        session.delete(comfort_level)
        session.commit()

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    return Response(status=200)

@comfort_level_controller.teardown_request
def shutdown_session(exception=None):
    Session.remove()