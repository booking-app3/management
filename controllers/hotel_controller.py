from flask import jsonify, Response, request, json, Blueprint
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError
from hotel import Hotel
from city import City
from utils import required_params
from session import Session

hotel_controller = Blueprint('hotel_controller', __name__, template_folder='controllers')

@hotel_controller.route("/api/hotels", methods=['GET'])
def get_hotels():
    session = Session()
    try:
        hotels = session.query(Hotel).all()
    except Exception as e:
        session.rollback()
        return str(e)
    return jsonify(hotels)

@hotel_controller.route("/api/hotels", methods=['POST'])
@required_params({"name": str, "stars": int, "city_id" : int})
def add_hotel():
    data = request.get_json(silent=True)
    session = Session()
    try:
        session.query(City).filter(City.id == data['city_id']).one()
        hotel = Hotel(hotel_name=data['name'],
                      stars=data['stars'],
                      city_id=data['city_id'])
        session.add(hotel)
        session.commit()

    except IntegrityError:
        session.rollback()
        return Response(status=409)

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        session.rollback()
        return str(e)

    return Response(json.dumps({'id': hotel.id}),
                    status=201,
                    mimetype='application/json')

@hotel_controller.route("/api/hotels", methods=['PUT'])
@required_params({"id": int, "name": str, "stars": int, "city_id" : int})
def update_hotel():
    data = request.get_json(silent=True)
    session = Session()
    try:
        session.query(City).filter(City.id == data['city_id']).one()
        hotel = session.query(Hotel).filter(Hotel.id == data['id']).one()
        hotel.name = data['name']
        hotel.stars = data['stars']
        hotel.city_id = data['city_id']
        session.commit()

    except IntegrityError:
        session.rollback()
        return Response(status=409)

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        session.rollback()
        return str(e)

    return Response(json.dumps({'id': hotel.id}),
                    status=201,
                    mimetype='application/json')

@hotel_controller.route("/api/hotels/<int:hotel_id>", methods=["DELETE"])
def delete_hotel(hotel_id):
    session = Session()
    try:
        hotel = session.query(Hotel).filter(Hotel.id == hotel_id).one()
        session.delete(hotel)
        session.commit()

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    return Response(status=200)

@hotel_controller.teardown_request
def shutdown_session(exception=None):
    Session.remove()

