from flask import jsonify, Response, request, json, Blueprint
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError
from utils import required_params
from session import Session
from facility import Facility
from comfort_level import ComfortLevel

facility_controller = Blueprint('facility_controller', __name__, template_folder='controllers')

@facility_controller.route("/api/facilities", methods=['GET'])
def get_facilities():
    session = Session()
    try:
        facilities = session.query(Facility).all()
    except Exception as e:
        session.rollback()
        return str(e)
    return jsonify(facilities)

@facility_controller.route("/api/facilities", methods=['POST'])
@required_params({"facility_name": str, "comfort_level_id": int})
def add_facility():
    data = request.get_json(silent=True)
    session = Session()
    try:
        session.query(ComfortLevel).filter(ComfortLevel.id == data['comfort_level_id']).one()
        facility = Facility(facility_name=data['facility_name'],
                      comfort_level_id=data['comfort_level_id'])
        session.add(facility)
        session.commit()

    except IntegrityError:
        session.rollback()
        return Response(status=409)

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        session.rollback()
        return str(e)

    return Response(json.dumps({'id': facility.id}),
                    status=201,
                    mimetype='application/json')

@facility_controller.route("/api/facilities", methods=['PUT'])
@required_params({"id": int, "facility_name": str, "comfort_level_id": int})
def update_facility():
    data = request.get_json(silent=True)
    session = Session()
    try:
        session.query(ComfortLevel).filter(ComfortLevel.id == data['comfort_level_id']).one()
        facility = session.query(Facility).filter(Facility.id == data['id'])
        facility.facility_name = data['facility_name']
        facility.comfort_level_id = data['comfort_level_id']
        session.commit()

    except IntegrityError:
        session.rollback()
        return Response(status=409)

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        session.rollback()
        return str(e)

    return Response(json.dumps({'id': facility.id}),
                    status=201,
                    mimetype='application/json')

@facility_controller.route("/api/facilities/<int:facility_id>", methods=['DELETE'])
def delete_facility(facility_id):
    session = Session()
    try:
        facility = session.query(Facility).filter(Facility.id == facility_id).one()
        session.delete(facility)
        session.commit()

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    return Response(status=200)

@facility_controller.teardown_request
def shutdown_session(exception=None):
    Session.remove()

