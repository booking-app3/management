from os import environ as env
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

engine = create_engine("mysql+mysqldb://{}:{}@{}:{}/{}".format(env['MYSQL_USER'],
                                                               env['MYSQL_PASSWORD'],
                                                               env['DATABASE_HOST'],
                                                               env['PORT'],
                                                               env['MYSQL_DATABASE']),
                       echo=True)

Session = scoped_session(sessionmaker(expire_on_commit=False))
Session.configure(bind=engine)