from flask import jsonify, Response, request, json, Blueprint
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError
from city import City
from utils import required_params
from session import Session

city_controller = Blueprint('city_controller', __name__, template_folder='controllers')

@city_controller.route("/api/cities", methods=['GET'])
def ger_cities():
    session = Session()
    try:
        cities = session.query(City).all()
    except Exception as e:
        session.rollback()
        return str(e)
    return jsonify(cities)

@city_controller.route("/api/cities", methods=['POST'])
@required_params({"name": str})
def add_city():
    data = request.get_json(silent=True)
    session = Session()
    try:
        city = City(city_name=data['name'])
        session.add(city)
        session.commit()

    except IntegrityError:
        session.rollback()
        return Response(status=409)

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        session.rollback()
        return str(e)

    return Response(json.dumps({'id': city.id}),
                    status=201,
                    mimetype='application/json')

@city_controller.route("/api/cities", methods=['PUT'])
@required_params({"id": int, "name": str})
def update_city():
    data = request.get_json(silent=True)
    session = Session()
    try:
        city = session.query(City).filter(City.id == data['id']).one()
        city.name = data['name']
        session.commit()

    except IntegrityError:
        session.rollback()
        return Response(status=409)

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        session.rollback()
        return str(e)

    return Response(json.dumps({'id': city.id}),
                    status=201,
                    mimetype='application/json')

@city_controller.route("/api/cities/<int:city_id>", methods=["DELETE"])
def delete_city(city_id):
    session = Session()
    try:
        city = session.query(City).filter(City.id == city_id).one()
        session.delete(city)
        session.commit()

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    return Response(status=200)

@city_controller.teardown_request
def shutdown_session(exception=None):
    Session.remove()