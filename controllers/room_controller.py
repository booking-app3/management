from flask import jsonify, Response, request, json, Blueprint
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError
from utils import required_params
from session import Session
from room import Room
from hotel import Hotel

room_controller = Blueprint('room_controller', __name__, template_folder='controllers')

@room_controller.route("/api/rooms", methods=['GET'])
def get_rooms():
    session = Session()
    try:
        rooms = session.query(Room).all()
    except Exception as e:
        session.rollback()
        return str(e)
    return jsonify(rooms)

@room_controller.route("/api/rooms", methods=['POST'])
@required_params({"room_floor": int, "hotel_id": int, "max_pers" : int, "description" : str})
def add_room():
    data = request.get_json(silent=True)
    session = Session()
    try:
        session.query(Hotel).filter(Hotel.id == data['hotel_id']).one()
        room = Room(room_floor=data['room_floor'],
                      hotel_id=data['hotel_id'],
                      max_pers=data['max_pers'],
					  description=data['description'])
        session.add(room)
        session.commit()

    except IntegrityError:
        session.rollback()
        return Response(status=409)

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        session.rollback()
        return str(e)

    return Response(json.dumps({'id': room.id}),
                    status=201,
                    mimetype='application/json')

@room_controller.route("/api/rooms", methods=['PUT'])
@required_params({"id": int, "room_floor": int, "hotel_id": int, "max_pers" : int, "description" : str})
def update_room():
    data = request.get_json(silent=True)
    session = Session()
    try:
        session.query(Hotel).filter(Hotel.id == data['hotel_id']).one()
        room = session.query(Room).filter(Room.id == data['id']).one()
        room.room_floor = data['room_floor']
        room.hotel_id = data['hotel_id']
        room.max_pers = data['max_pers']
        room.description = data['description']
        session.commit()

    except IntegrityError:
        session.rollback()
        return Response(status=409)

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        session.rollback()
        return str(e)

    return Response(json.dumps({'id': room.id}),
                    status=201,
                    mimetype='application/json')

@room_controller.route("/api/rooms/<int:room_id>", methods=['DELETE'])
def delete_room(room_id):
    session = Session()
    try:
        room = session.query(Room).filter(Room.id == room_id).one()
        session.delete(room)
        session.commit()

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    return Response(status=200)

@room_controller.teardown_request
def shutdown_session(exception=None):
    Session.remove()

