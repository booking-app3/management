# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## 2021-05-05

### Added

- Add Dokerfile and requirements.txt
- Add City and Hotel models
- Add management api and Flask app
- Add management.yml to run the service
- Add .gitlab-ci.yml - not working yet

## 2021-05-06

### Added

- utils.py for @required_params parsing
- gitlab registry for management api image

### Fixed

- Structure: use only one session, add a Blueprint for each controller
- Moved management.yml content to main stack from Configs repo
- .gitlab-ci.yml - test login, build and push for the image to gitlab registry

## 2021-05-07

- Add debug param in app.py
- Add Room, Facility and ComfortLevel models and controllers

## 2021-05-23

### Added

- Update and delete methods for each entity
